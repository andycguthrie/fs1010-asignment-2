import ToDoItem from "./ToDoItem"
import {useState} from "react";

const ToDoList = () => {
    const [toDoList, setToDoList] = useState([
        {item: "vacuum"},
        {item: "laundry"},
    ]);

    const [toDoItem, setToDoItem] = useState ({ item: ""});

    const handleChange =(event) => {
        setToDoItem ((prevState) => ({
            prevState,
            [event.target.name]: event.target.value,
        }));
    };
    const handleSubmit = (event) => {
        event.preventDefault();
        setToDoList((oldArray) => [ ...oldArray, toDoItem]);
        setToDoItem({item: ""});
       
    }

    return(
        <div>
        <form onSubmit={handleSubmit}>
        <input type="text" 
        placeholder="Add an item..." 
        onChange={handleChange} 
        value={toDoItem.item}
        name="item"
        /> 
      </form>

      <ul className="listItemContainer">
          {toDoList.map((itemObject) =>(
           <li key={itemObject.item} className="listRow">
               <ToDoItem item={itemObject.item}/>
            </li> 
          ))}
      </ul>
        </div>
       
    )
}

export default ToDoList