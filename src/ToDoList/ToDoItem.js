import {useState} from "react";
import checkmark from '../images/square-check-solid.svg';
import nocheckmark from '../images/square-nocheck-solid.svg';

const ToDoItems = (props) => {
    const [completed, setComplete] = useState(false);
    const handleComplete = () => {
        setComplete(true);
    }
    if(completed===false)
   {return (
    <div> <img src={nocheckmark} className="checkmarkChecked"onClick={handleComplete} />
        
       <li 
      className="listRowAbove"
      onClick={handleComplete}>
          
      {props.item }</li></div>      
    )}
    return(
 
      <div> <img src={checkmark} className="checkmarkChecked"/>
        <li 
        className="listItemDone">

        {props.item }</li></div>    
    )   
}

export default ToDoItems