import './App.css';
import ToDoList from './ToDoList/ToDoList';
import logo from '../src/images/logo.svg';

function App() {
  return (
  <div className="App">
  <div className="page-header"> 
  <div className="logo-container">  <img src={logo} className="App-logo"/> 
  <h1 className="orange">Get</h1>
  <h1 className="grey">Done</h1></div>
    
  </div>

  <ToDoList/>

  </div>
  );
}

export default App;
